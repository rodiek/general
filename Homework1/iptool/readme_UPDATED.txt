Andrew Rodiek U84332681
*** Note to TA ***
Functions and parameters have been updated in this list.
Sample input file: "../iptool/input/SampleParameters" This has been assigned in the solution
The file "../iptool/input/InputFormat" shows the input with some explaination in addtion to what is listed below



This software is architectured as follows

iptool/iptool.cpp- This file includes the main function.

iptool/iptools -This folder hosts the files that are compiled into a static library. 
	image - This folder hosts the files that define an image.
	utility- this folder hosts the files that students store their implemented algorithms.

*** INSTALATION ***

On Windows

Open the project by double click iptool.sln.

*** FUNCTIONS ***

1. Add intensity: add
Increase the intensity for a gray-level image.

2. Binarization: binarize
Binarize the pixels with the threshold.

3. Scaling: Scale
Reduce or expand the heigh and width with twp scale factors.
Scaling factor = 2: double height and width of the input image.
Scaling factor = 0.5: half height and width of the input image.

4. Binarization (two value): twot
Binarize pixels with two thresholds

5. Color intensity: colormod
Adjust intensity of RGB values.

*** PARAMETERS ***

The first parameter of the parameters.txt is the number of operations (lines).
There are four parameters for each operation (line):
1. the input file name;
2. the output file name;
3. the name of the filter. Use "add", "binarize", "scale", "twot", "colormod" for your filters;
4. the value for adding intensity, threshold value for binarize filter, or the scaling factor for scale filter, or the number of ROI's
(optional for ROI)
5. x coord for roi
6. y coord for roi
7. width of roi
8. height of roi
(additional arguments for two new functions)
colormod arguments:
	9. red value
	10. blue value
	11. green value
twot arguments:
	9. lower threshold
	10. upper threshold


*** Run the program:

Directly debug in Visual Studio.
You can find the output image in output folder.


*** Important information ***

Application assumes the next format of input image (ppm/pgm) file:
line1: <version>
line2: <#columns> <#rows>
line3: <max_value>
line4-end-of-file:<pix1><pix2>...<pix_n>

if it is a grayscale image then every pixel is a char value. If it is a RGB image then every pixel is a set of 3 char values: <red><green><blue>

Thus, if you have a problem with reading image, the first thing you should check is input file format.
