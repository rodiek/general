/************************************************************
 *															*
 * This sample project include three functions:				*
 * 1. Add intensity for gray-level image.					*
 *    Input: source image, output image name, value			*
 *															*
 * 2. Image thresholding: pixels will become black if the	*
 *    intensity is below the threshold, and white if above	*
 *    or equal the threhold.								*
 *    Input: source image, output image name, threshold		*
 *															*
 * 3. Image scaling: reduction/expansion of 2 for 			*
 *    the width and length. This project uses averaging 	*
 *    technique for reduction and pixel replication			*
 *    technique for expansion.								*
 *    Input: source image, output image name, scale factor	*
 *															*
 ************************************************************/

#include "./iptools/core.h"
#include "roi.h"
#include <string.h>
#include <fstream>

using namespace std;

#define MAXLEN 256

int main (int argc, char** argv)
{
	image src, tgt;
	ifstream fp(argv[1]);
	char str[MAXLEN];
	rsize_t strmax = sizeof str;
	char outfile[MAXLEN];
	char *pch, *next_pch;
	int nOP;
	if (!fp.is_open()) {
		fprintf(stderr, "Can't open file: %s\n", argv[1]);
		exit(1);
	}

	fp >> nOP;
	for (int OP = 0; OP < nOP; OP++) {
		fp >> str;
		src.read(str);

		fp >> str;
		strcpy_s(outfile, MAXLEN, str);

		fp >> str;
        if (strncmp(str,"add",3)==0) {
			/* Add Intensity */
			fp >> str;
        	utility::addGrey(src,tgt,atoi(str));
        }

        else if (strncmp(str,"binarize",8)==0) {
			/* Thresholding */
			fp >> str;
			utility::binarize(src,tgt,atoi(str));
		}

		else if (strncmp(str,"scale",5)==0) {
			/* Image scaling */
			fp >> str;
			utility::scale(src,tgt,atof(str));
		}

		else if (strncmp(str, "twot", 4) == 0) {
			/* Two Value Thresholding */
			fp >> str;
			int numROI = atoi(str);
			roi temp;
			int t1, t2;
			// For loop handles ROI and arguments 
			// I will separate this into another function at a later time, but this will have to work for now
			for (int i = 0; i < numROI; i++)
			{
				for (int i = 0; i <= 5; i++)
				{
					fp >> str;
					switch (i) {
						case 0:
							temp.x = atoi(str);
							break;
						case 1:
							temp.y = atoi(str);
							break;
						case 2:
							temp.sx = atoi(str);
							break;
						case 3:
							temp.sy = atoi(str);
							break;
						// Get threshold bounds
						case 4:
							t1 = atoi(str);
							break;
						case 5:
							t2 = atoi(str);
							continue;							
					}
				}
				if(i > 0)
					utility::dGrayThresholding(tgt, tgt, temp, t1, t2);
				else
					utility::dGrayThresholding(src, tgt, temp, t1, t2);
			}
		}

		/*
			NOT WORKING
		*/
		//else if (strncmp(str, "smooth1d", 8) == 0) {
		//	/* Two Value Thresholding */
		//	fp >> str;
		//	int numROI = atoi(str);
		//	roi temp;
		//	int t1, t2;
		//	for (int i = 0; i < numROI; i++)
		//	{
		//		for (int i = 0; i <= 6; i++)
		//		{
		//			fp >> str;
		//			switch (i) {
		//			case 0:
		//				temp.x = atoi(str);
		//				break;
		//			case 1:
		//				temp.y = atoi(str);
		//				break;
		//			case 2:
		//				temp.sx = atoi(str);
		//				break;
		//			case 3:
		//				temp.sy = atoi(str);
		//				break;
		//			case 4:
		//				t1 = atoi(str);
		//				break;
		//			case 5:
		//				t2 = atoi(str);
		//				break;
		//			}
		//		}
		//		// I know this is gross...
		//		if (i > 0)
		//			utility::dGrayThresholding(tgt, tgt, temp, t1, t2);
		//		else
		//			utility::dGrayThresholding(src, tgt, temp, t1, t2);
		//	}
		//}


		else if (strncmp(str, "colormod", 8) == 0) {
			/* Color Modification */
			fp >> str;
			int numROI = atoi(str);
			roi temp;
			int dR, dG, dB;
			// For loop handles ROI and arguments 
			// I will separate this into another function at a later time, but this will have to work for now
			for (int i = 0; i < numROI; i++)
			{
				for (int i = 0; i <= 6; i++)
				{
					fp >> str;
					switch (i) {
					case 0:
						temp.x = atoi(str);
						break;
					case 1:
						temp.y = atoi(str);
						break;
					case 2:
						temp.sx = atoi(str);
						break;
					case 3:
						temp.sy = atoi(str);
						break;
					// get color arguments
					case 4:
						dR = atoi(str);
						break;
					case 5:
						dG = atoi(str);
						break;
					case 6:
						dB = atoi(str);
						break;
					}
				}
				if (i > 0)
					utility::colorMod(tgt, tgt, temp, dR, dG, dB);
				else
					utility::colorMod(src, tgt, temp, dR, dG, dB);
			}
		}

		else {
			printf("No function: %s\n", str);
			continue;
		}
       
		tgt.save(outfile);
	}
	//fclose(fp);
	fp.close();
	return 0;
}

