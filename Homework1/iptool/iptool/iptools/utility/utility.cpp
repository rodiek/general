#include "utility.h"
#include "roi.h"

#define MAXRGB 255
#define MINRGB 0

std::string utility::intToString(int number)
{
   std::stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}

int utility::checkValue(int value)
{
	if (value > MAXRGB)
		return MAXRGB;
	if (value < MINRGB)
		return MINRGB;
	return value;
}

/*-----------------------------------------------------------------------**/
void utility::addGrey(image &src, image &tgt, int value)
{
	tgt.resize(src.getNumberOfRows(), src.getNumberOfColumns());
	for (int i=0; i<src.getNumberOfRows(); i++)
		for (int j=0; j<src.getNumberOfColumns(); j++)
		{
			tgt.setPixel(i,j,checkValue(src.getPixel(i,j)+value)); 
		}
}

/*-----------------------------------------------------------------------**/
void utility::binarize(image &src, image &tgt, int threshold)
{
	tgt.resize(src.getNumberOfRows(), src.getNumberOfColumns());
	for (int i=0; i<src.getNumberOfRows(); i++)
	{
		for (int j=0; j<src.getNumberOfColumns(); j++)
		{
			if (src.getPixel(i,j) < threshold)
				tgt.setPixel(i,j,MINRGB);
			else
				tgt.setPixel(i,j,MAXRGB);
		}
	}
}

/*-----------------------------------------------------------------------**/
void utility::scale(image &src, image &tgt, float ratio)
{
	int rows = (int)((float)src.getNumberOfRows() * ratio);
	int cols  = (int)((float)src.getNumberOfColumns() * ratio);
	tgt.resize(rows, cols);
	for (int i=0; i<rows; i++)
	{
		for (int j=0; j<cols; j++)
		{	
			/* Map the pixel of new image back to original image */
			int i2 = (int)floor((float)i/ratio);
			int j2 = (int)floor((float)j/ratio);
			if (ratio == 2) {
				/* Directly copy the value */
				tgt.setPixel(i,j,checkValue(src.getPixel(i2,j2)));
			}

			if (ratio == 0.5) {
				/* Average the values of four pixels */
				int value = src.getPixel(i2,j2) + src.getPixel(i2,j2+1) + src.getPixel(i2+1,j2) + src.getPixel(i2+1,j2+1);
				tgt.setPixel(i,j,checkValue(value/4));
			}
		}
	}
}

/*-----------------------------------------------------------------------**/
void utility::dGrayThresholding(image &src, image &tgt, roi troi, int threshold1, int threshold2)
{
	// make a copy of image source in case of multiple ROI
	image temp = src;
	for (int i = 0; i<troi.sx; i++)
	{
		for (int j = 0; j<troi.sy; j++)
		{
			if (src.getPixel(troi.x + i, troi.y + j) >= threshold1 && src.getPixel(troi.x + i, troi.y + j) <= threshold2)
				temp.setPixel(troi.x + i, troi.y + j, MINRGB);
			else
				temp.setPixel(troi.x + i, troi.y + j, MAXRGB);
		}
	}
	tgt = temp;
}

/*-----------------------------------------------------------------------**/
void utility::Smooth1d(image &src, image &tgt, roi troi, int ws)
{
	/*
		NOT WORKING
	*/

	// make a copy of image source in case of multiple ROI
	//image temp = src;
	//for (int i = 0; i<troi.sx; i++)
	//{
	//	for (int j = 0; j<troi.sy; j++)
	//	{
	//		temp.setPixel(troi.x + i, troi.y + j, src.getPixel(troi.x + i, troi.y + j) * ws);
	//	}
	//}
	//tgt = temp;
}

void utility::colorMod(image &src, image &tgt, roi troi, int dR, int dG, int dB)
{
	// make a copy of image source in case of multiple ROI
	image temp = src;
	for (int i = 0; i < troi.sx; i++)
	{
		for (int j = 0; j < troi.sy; j++)
		{
			// error checking out of bounds for RGB
			int nR = src.getPixel(troi.x + i, troi.y + j, RED) + dR;
			if (nR > MAXRGB)
				nR = MAXRGB;
			else if (nR < MINRGB)
				nR = MINRGB;

			int nG = src.getPixel(troi.x + i, troi.y + j, GREEN) + dG;
			if (nG > MAXRGB)
				nG = MAXRGB;
			else if (nG < MINRGB)
				nG = MINRGB;
			
			int nB = src.getPixel(troi.x + i, troi.y + j, BLUE) + dB;
			if (nB > MAXRGB)
				nB = MAXRGB;
			else if (nB < MINRGB)
				nB = MINRGB;

			// Set new values
			temp.setPixel(troi.x + i, troi.y + j, nR);
			temp.setPixel(troi.x + i, troi.y + j, nG);
			temp.setPixel(troi.x + i, troi.y + j, nB);
		}
	}
	tgt = temp;
}
