#ifndef UTILITY_H
#define UTILITY_H

#include "../image/image.h"
#include <sstream>
#include <math.h>
#include "roi.h"

class utility
{
	public:
		utility();
		virtual ~utility();
		static std::string intToString(int number);
		static int checkValue(int value);
		static void addGrey(image &src, image &tgt, int value);
		static void binarize(image &src, image &tgt, int threshold);
		static void scale(image &src, image &tgt, float ratio);
		static void dGrayThresholding(image &src, image &tgt, roi troi, int threshold1, int threshold2);
		static void Smooth1d(image &src, image &tgt, roi troi, int ws);
		static void colorMod(image &src, image &tgt, roi troi, int dR, int dG, int dB);
};

#endif

