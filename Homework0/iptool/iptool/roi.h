#ifndef ROI_H
#define ROI_H

class roi
{
public:
	int x;
	int y;
	int sx;
	int sy;

	roi(int ix, int iy, int isx, int isy)
	{
		x = ix;
		y = iy;
		sx = isx;
		sy = isy;
	}

};

#endif // !ROI_H

