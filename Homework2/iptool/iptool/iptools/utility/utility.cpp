#include "utility.h"
#include "roi.h"
#include <math.h>
#include <array>

#define MAXRGB 255
#define MINRGB 0
//#define PI 3.1415926535897;
const double PI = 3.1415926535897;

std::string utility::intToString(int number)
{
   std::stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}

int utility::checkValue(int value)
{
	if (value > MAXRGB)
		return MAXRGB;
	if (value < MINRGB)
		return MINRGB;
	return value;
}

/*-----------------------------------------------------------------------**/
void utility::addGrey(image &src, image &tgt, int value)
{
	tgt.resize(src.getNumberOfRows(), src.getNumberOfColumns());
	for (int i=0; i<src.getNumberOfRows(); i++)
		for (int j=0; j<src.getNumberOfColumns(); j++)
		{
			tgt.setPixel(i,j,checkValue(src.getPixel(i,j)+value)); 
		}
}

/*-----------------------------------------------------------------------**/
void utility::binarize(image &src, image &tgt, int threshold)
{
	tgt.resize(src.getNumberOfRows(), src.getNumberOfColumns());
	for (int i=0; i<src.getNumberOfRows(); i++)
	{
		for (int j=0; j<src.getNumberOfColumns(); j++)
		{
			if (src.getPixel(i,j) < threshold)
				tgt.setPixel(i,j,MINRGB);
			else
				tgt.setPixel(i,j,MAXRGB);
		}
	}
}

/*-----------------------------------------------------------------------**/
void utility::scale(image &src, image &tgt, float ratio)
{
	int rows = (int)((float)src.getNumberOfRows() * ratio);
	int cols  = (int)((float)src.getNumberOfColumns() * ratio);
	tgt.resize(rows, cols);
	for (int i=0; i<rows; i++)
	{
		for (int j=0; j<cols; j++)
		{	
			/* Map the pixel of new image back to original image */
			int i2 = (int)floor((float)i/ratio);
			int j2 = (int)floor((float)j/ratio);
			if (ratio == 2) {
				/* Directly copy the value */
				tgt.setPixel(i,j,checkValue(src.getPixel(i2,j2)));
			}

			if (ratio == 0.5) {
				/* Average the values of four pixels */
				int value = src.getPixel(i2,j2) + src.getPixel(i2,j2+1) + src.getPixel(i2+1,j2) + src.getPixel(i2+1,j2+1);
				tgt.setPixel(i,j,checkValue(value/4));
			}
		}
	}
}

/*-----------------------------------------------------------------------**/
void utility::dGrayThresholding(image &src, image &tgt, roi troi, int threshold1, int threshold2)
{
	// make a copy of image source in case of multiple ROI
	image temp = src;
	for (int i = 0; i<troi.sx; i++)
	{
		for (int j = 0; j<troi.sy; j++)
		{
			if (src.getPixel(troi.x + i, troi.y + j) >= threshold1 && src.getPixel(troi.x + i, troi.y + j) <= threshold2)
				temp.setPixel(troi.x + i, troi.y + j, MINRGB);
			else
				temp.setPixel(troi.x + i, troi.y + j, MAXRGB);
		}
	}
	tgt = temp;
}

/*-----------------------------------------------------------------------**/
void utility::Smooth1d(image &src, image &tgt, roi troi, int ws)
{
	/*
		NOT WORKING
	*/

	// make a copy of image source in case of multiple ROI
	//image temp = src;
	//for (int i = 0; i<troi.sx; i++)
	//{
	//	for (int j = 0; j<troi.sy; j++)
	//	{
	//		temp.setPixel(troi.x + i, troi.y + j, src.getPixel(troi.x + i, troi.y + j) * ws);
	//	}
	//}
	//tgt = temp;
}

/*-----------------------------------------------------------------------**/
void utility::colorMod(image &src, image &tgt, roi troi, int dR, int dG, int dB)
{
	// make a copy of image source in case of multiple ROI
	image temp = src;
	for (int i = 0; i < troi.sx; i++)
	{
		for (int j = 0; j < troi.sy; j++)
		{
			// error checking out of bounds for RGB
			int nR = src.getPixel(troi.x + i, troi.y + j, RED) + dR;
			if (nR > MAXRGB)
				nR = MAXRGB;
			else if (nR < MINRGB)
				nR = MINRGB;

			int nG = src.getPixel(troi.x + i, troi.y + j, GREEN) + dG;
			if (nG > MAXRGB)
				nG = MAXRGB;
			else if (nG < MINRGB)
				nG = MINRGB;
			
			int nB = src.getPixel(troi.x + i, troi.y + j, BLUE) + dB;
			if (nB > MAXRGB)
				nB = MAXRGB;
			else if (nB < MINRGB)
				nB = MINRGB;

			// Set new values
			temp.setPixel(troi.x + i, troi.y + j, nR);
			temp.setPixel(troi.x + i, troi.y + j, nG);
			temp.setPixel(troi.x + i, troi.y + j, nB);
		}
	}
	tgt = temp;
}

/*-----------------------------------------------------------------------**/
// Histogram creation. Will output histogram with target name+ _Hist
// String manipulation from: 
//   https://www.safaribooksonline.com/library/view/c-cookbook/0596007612/ch10s17.html
void utility::MakeHist(int arr[], int pix, string opName, bool og)
{
	int max = 0;
	for (int p = 0; p < 255; p++)
	{
		if (arr[p] > max)
			max = arr[p];
	}

	// Make histogram
	image hist = image(max, 256);
	for (int x = 0; x < 256; x++)
	{
		for (int y = 0; y < max; y++)
		{
			// Setting bars
			if (arr[x] >= (max - y))
				hist.setPixel(y, x, 255);
			else
				hist.setPixel(y, x, 0);
		}
	}

	// Set histogram output name
	string addExt = "_histogram";
	if (og)
		addExt += "_OG";
	else
		addExt += "_New";
	string ext;
	char finName[1024];
	string::size_type i = opName.rfind('.', opName.length());
	if (i != string::npos)
	{
		ext = opName.substr(i, opName.length());
		opName.replace(i, addExt.length() + ext.length(), addExt + ext);
	}

	// Output name must be char array
	strncpy_s(finName, opName.c_str(), sizeof(finName));
	hist.save(finName);
}
/*-----------------------------------------------------------------------**/
void utility::HistStretch(image &src, image &tgt, roi temp, int a, int b, int c, string outputname)
{
	int ogH[256] = { 0 };
	int nwH[256] = { 0 };

	// make a copy of image source in case of multiple ROI
	image tmpImg = src;
	for (int i = temp.x; i < (temp.x + temp.sx); i++)
	{
		for (int j = temp.y; j < (temp.y + temp.sy); j++)
		{
			int np = 0;
			// Get value of pixel
			int op = src.getPixel(j, i);

			// add value to histogram array
			ogH[op] += 1;

			//new value calculation
			if (op < a && op >= 0)
				np = c;
			else if (op >= a && op <= b)
				np = ((255 - c) / (b - a)) * (op - a) + c;
			else if (op >= c && op < 255)
				np = 255;
			else
			{
				printf("Something went horribly wrong in HistStretch");
				exit(-1);
			}

			// Just in case
			if (np > 255)
				np = 255;
			if (np < 0)
				np = 0;

			// add value to new histogram array
			nwH[np] += 1;
			// set the value in the new image
			tmpImg.setPixel(j, i, np);
		}
	}
	// Create histograms
	int test = temp.sx * temp.sy;
	MakeHist(ogH, temp.sx * temp.sy, outputname, true);
	MakeHist(nwH, temp.sx * temp.sy, outputname, false);
	tgt = tmpImg;
}

/*-----------------------------------------------------------------------**/
void utility::RGBtoHSI(int iR, int iG, int iB, double (&arr)[3])
{
	double tot = iR + iG + iB;
	double r = iR / tot;
	double g = iG / tot;
	double b = iB / tot;
	double H;

	if (iR == iG && iG == iB)
	{
		arr[0] = 0;
		arr[1] = 0;
		arr[2] = tot / (3 * 255);
		return;
	}

	// H calculation
	double calc = (0.5 * ((r - g) + (r - b))) / pow((pow((r - g), 2) + ((r - b) * (g - b))), .5);
	double theta;

	if (calc == 1) {
		theta = 0;
	}
	else if (calc == -1) {
		theta = PI;
	}
	else {
		theta = acos(calc);
	}

	if (b > g) {
		H = 2 * PI - theta;
	}
	else {
		H = theta;
	}

	// S calculation
	double S = 1 - (3 * min(min(r, g), b));

	// I calculation
	double I = tot / (3 * 255);

	arr[0] = H * 180 / PI;
	arr[1] = S * 100;
	arr[2] = I * 255;

	return;
}

/*-----------------------------------------------------------------------**/
void utility::HSItoRGB(double (&hsi)[3], int (&rgb)[3]) 
{
	double x, y, z;
	double calc;
	double H = hsi[0] * PI / 180;
	double S = hsi[1] / 100;
	double I = hsi[2] / 255;
	x = I * (1 - S);

	if (H < ((2 * PI) / 3)) {
		// calculate values
		y = I * (1 + ((S * cos(H)) / cos((PI / 3) - H)));
		z = (3 * I) - (x + y);

		// Set values
		rgb[0] = y * 255;
		rgb[1] = z * 255;
		rgb[2] = x * 255;
	}
	else if (((2 * PI) / 3) <= H && H < (((4 * PI) / 3))) {
		// calculate values
		H = H - ((2 * PI) / 3);
		y = I * (1 + ((S * cos(H)) / cos((PI / 3) - H)));
		z = (3 * I) - (x + y);

		// Set values
		rgb[0] = x * 255;
		rgb[1] = y * 255;
		rgb[2] = z * 255;
	}
	else if (((4 * PI) / 3) <= H && H < (2 * PI)) {
		// calculate values
		H = H - ((4 * PI) / 3);
		y = I * (1 + ((S * cos(H)) / cos((PI / 3) - H)));
		z = (3 * I) - (x + y);

		// Set values
		rgb[0] = z * 255;
		rgb[1] = x * 255;
		rgb[2] = y * 255;
	}
	else
	{
		printf("Something went horribly wrong with HSI to RGB conversion");
		exit(-1);
	}

	return;
}
/*-----------------------------------------------------------------------**/
void utility::RGBconvert(image &src, image &tgt, roi temp, int a, int b, int d, int e)
{
	// make a copy of image source in case of multiple ROI
	image tmpImg = src;

	double ogHSI[3] = { 0 };
	int newRGB[3] = { 0 };
	for (int i = temp.x; i < ( temp.x + temp.sx); i++)
	{
		for (int j = temp.y; j <( temp.y + temp.sy); j++)
		{
			// Convert to HSI
			int R = src.getPixel(i, j, RED);
			int G = src.getPixel(i, j, GREEN);
			int B = src.getPixel(i, j, BLUE);
			RGBtoHSI(R, G, B, ogHSI);

			// Stretch
			//ogHSI[2] = ((ogHSI[2] - d) * ((e - d) / (b - a))) + a;
			ogHSI[2] = ((e - d) / (b - a)) * (ogHSI[2] - a) + d;
			// check bounds
			if (ogHSI[2] > 255)
				ogHSI[2] = 255;
			if (ogHSI[2] < 0)
				ogHSI[2] = 0;
			// Convert back to RGB
			HSItoRGB(ogHSI, newRGB);
			tmpImg.setPixel(i, j, RED, newRGB[0]);
			tmpImg.setPixel(i, j, GREEN, newRGB[1]);
			tmpImg.setPixel(i, j, BLUE, newRGB[2]);
		}
	}

	tgt = tmpImg;
}
/*-----------------------------------------------------------------------**/
void utility::BiHistStretch(image &src, image &tgt, roi temp, int(&input)[6], string outputname)
{
	int ogH[256] = { 0 };
	int nwH[256] = { 0 };

	// make a copy of image source in case of multiple ROI
	image tmpImg = src;
	for (int i = temp.x; i < (temp.x + temp.sx); i++)
	{
		for (int j = temp.y; j < (temp.y + temp.sy); j++)
		{
			int np = 0;
			// Get value of pixel
			int op = src.getPixel(i, j);

			// add value to histogram array
			ogH[op] += 1;

			// new value calculation
			if (0 <= op && op < input[0])
				np = input[3];
			else if (input[0] <= op && op < input[1])
				np = ((input[4] - input[3]) / (input[1] - input[0])) * (op - input[0]) + input[3];
			else if (input[1] <= op && op < input[2])
				np = ((input[5] - input[4]) / (input[2] - input[1])) * (op - input[1]) + input[4];
			else if (input[2] <= op && op <= 255)
				np = input[5];
			else
			{
				printf("Something went horribly wrong in BiHist");
				exit(-1);
			}

			// Just in case
			if (np > 255)
				np = 255;
			if (np < 0)
				np = 0;

			// add value to new histogram array
			nwH[np] += 1;
			// set the value in the new image
			tmpImg.setPixel(i, j, np);
		}
	}
	// Create histograms
	int test = temp.sx * temp.sy;
	MakeHist(ogH, temp.sx * temp.sy, outputname, true);
	MakeHist(nwH, temp.sx * temp.sy, outputname, false);
	tgt = tmpImg;
}