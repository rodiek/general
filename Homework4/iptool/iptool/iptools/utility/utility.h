#ifndef UTILITY_H
#define UTILITY_H

#include "../image/image.h"
#include <sstream>
#include <math.h>
#include "roi.h"

class utility
{
	public:
		utility();
		virtual ~utility();
		static std::string intToString(int number);
		static int checkValue(int value);
		static void addGrey(image &src, image &tgt, int value);
		static void binarize(image &src, image &tgt, int threshold);
		static void scale(image &src, image &tgt, float ratio);
		static void dGrayThresholding(image &src, image &tgt, roi troi, int threshold1, int threshold2);
		static void Smooth1d(image &src, image &tgt, roi troi, int ws);
		static void colorMod(image &src, image &tgt, roi troi, int dR, int dG, int dB);
		static void HistStretch(image &src, image &tgt, roi temp, int a, int b, int c, string outputname);
		static void MakeHist(int arr[], int pix, string opName, bool og);
		static void RGBconvert(image &src, image &tgt, roi temp, int a, int b, int d, int e);
		static void RGBtoHSI(int iR, int iG, int iB, double (&arr)[3]);
		static void HSItoRGB(double (&hsi)[3], int(&rgb)[3]);
		static void BiHistStretch(image &src, image &tgt, roi temp, int(&input)[6], string outputname);
		static void EdgeDetetction(image &src, image &tgt, roi temp, int t, string Op, string outName, int angle);
		static void OpenCVHistStretch(char *infile, char* outfile, roi temp);
};

#endif

